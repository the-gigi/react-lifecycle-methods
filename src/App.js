import React, {Component} from 'react'
import './App.css'
import PopularBar from "./components/PopularBar"

class App extends Component {
  constructor() {
    super()
    this.state = {
      showPopularBar: true,
      upCount: 5,
      downCount: 8
    }
  }

  componentDidMount() {
    this.timer = setInterval(this.everyFiveSeconds.bind(this), 5000);
  }

  everyFiveSeconds() {
    let state = this.state
    state.upCount += 20
    this.setState(state)
  }

  render() {
    return (
      <div>
        <h1>Popular Bar</h1>
        <label>
          <input
            type='checkbox'
            defaultChecked={this.state.showPopularBar}
            ref='showPopularBar'
            onChange={() => this.setState({showPopularBar: !this.state.showPopularBar })
            }
          />
          Show popular bar
        </label>

        <PopularBar
            show={this.state.showPopularBar}
            upCount={this.state.upCount}
            downCount={this.state.downCount}
        />
      </div>
    )
  }
}

export default App
